const mysql = require('mysql2');

/**
   * @param {string} [host] - hostname
   * @param {string} [user] - username
   * @param {string} [password] - password related to user 
   * @param {string} [database] - database you want 
   * @return this connection.
   */
module.exports = function (host, user, password, database) {
    return mysql.createConnection({
        host: host,
        user: user,
        password: password,
        database: database
    })
}