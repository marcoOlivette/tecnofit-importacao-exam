const con = require('./CreateConnection')

module.exports = class Cliente{

  /**
   * 
   * @param {con} connection - connection to take datas from exam-backup database
   * @param {con} connection2 - connection to put data from a database 
   */
  constructor(connection, connection2) {
    this.connection = connection
    this.connection2 = connection2
  }

  /**
   * 
   * @param {string} nome 
   * @param {string} email 
   * @param {string} cpf 
   */
  insertIntoCliente(nome, email, cpf){
    this.connection2.query(
      'insert into cliente (nome, email, cpf) values(?, ?, ?)',[nome, email, cpf],
      (error) => {
        if(error) console.log(error);
      }
    );
  }

  /**
   * Insert into table cliente name, cpf and email
   */
  selectToCliente(){
    this.connection.query(
      'select nome, `e-mail`, cpf from usuarios',
      (error, results) => {
        if(error) console.log(error);
        for(let i = 0; i < results.length; i++){
          this.insertIntoCliente(results[i].nome,results[i].email,results[i].cpf)
        }
      }
    )
  }  

  deleteDataTable(){
    this.connection2.query(
      'delete from cliente',
      (error, results) => {
        if(error) console.log(error);
      }
    )
  }
}
