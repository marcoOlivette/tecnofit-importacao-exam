const con = require('./CreateConnection')

module.exports = class Cliente{

  /**
   * 
   * @param {con} connection - connection to take datas from exam-backup database
   * @param {con} connection2 - connection to put data from a database 
   */
  constructor(connection, connection2) {
    this.connection = connection
    this.connection2 = connection2
  }

  selectData(){
      this.connection.query(
          'select taxa as desconto,' +
          'valor_liquido as valor,' +
          'data_do_vencimento as dataVenc,' + 
          'data_do_recebimento as DataRec,' +
          'descricao '+
          'from contas_a_receber limit 1', 
          (error, result) => {
            if(error) console.log(error);
            for(let i = 0; i < result.length; i++){
                this.tempDataVencimento = 'STR_TO_DATE('+ result[i].dataVenc.trim() + ', %d/%m/%Y %H:%i:%s )'
                this.tempDataRecebimento = 'STR_TO_DATE('+ result[i].DataRec.trim() + ', %d/%m/%Y %H:%i:%s )'
                this.tempDescricao = result[i].descricao+'';
                this.parcela = this.tempDescricao.split(" ",1) +''
                this.parcelaAtual = parseInt(this.parcela[0], 10)
                this.totalParcela = parseInt(this.parcela[2] + this.parcela[3], 10)
                this.desconto = parseFloat(result[i].desconto.trim(), 10)
                this.valor = parseFloat(result[i].valor.trim(), 10)
                this.insertoData(this.valor, this.desconto, this.tempDataRecebimento, 
                    this.tempDataVencimento, this.totalParcela, this.parcelaAtual)
            }
          }
      )
  }

  insertoData(valor, valor_desconto, data_recebimento, data_vencimento, total_parcelas, parcela_atual){
      this.connection2.query(
          'insert into lancamentos' +
          '(valor, valor_desconto, data_recebimento, '+
          'data_vencimento, total_parcelas, parcela_atual)'+
          'values (?,?,?,?,?,?)',
          [valor, valor_desconto, data_recebimento, data_vencimento, total_parcelas, parcela_atual],
          (error) => {
            if(error) console.log(error);
          }
      )
  }
}