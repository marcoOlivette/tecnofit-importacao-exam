# Comentários da avaliação
## Etapa 1 - deduções sobre o banco de dados
Primeira coisa que fiz foi comparar as mudanças do backup e a base nova que será implementada. 

#### Tabela cliente
- **nome**, **email** e **cpf** virá de **nome**, **email** e **cpf** de **usuarios** do backup

- Campo **codigo_importacao** do modelo novo era o **código_de_venda** da **relatorio_vendas** do backup

#### Tabela lancamentos

- Entendi que o **banco** da tabela **contas_a_receber** da base antiga poderia ser o **tipo_lancamento**, e **tipo_recebimento** a **forma_pagto** da tabela **contas_a_receber** da base antiga. Iria fazer cada banco ser um número diferente, por exemplo, Bradesco = 1. Forma de pagamento também seguiria essa lógica, por exemplo, Cartão de Crédito = 1 e Cheque = 2.

- **Valor** virá de  **valor_liquido** de **contas_a_receber**

- **valor_desconto** virá de **taxa** de contas_a_receber

- Não entendi onde viria a **data_lancamento**.

- **data_recebimento** virá de **data_do_recebimento** de **contas_a_receber**

- **data_lancamento** virá de **data_do_lancamento** de **contas_a_receber**

- **total_parcelas** e **parcela_atual** virá do campo **descricao**

#### Tabela contrato

- campo **nome** virá de da tabela **cliente** invés de acessar novamente o banco de backup.

- campo **valor_padrão** deduzi que seria o campo **valor_bruto** da tabela contrato do backup.

- Os outros campos não compreendi

#### Tabela cliente contrato  e cliente venda ficaram muito confusas sem alguem para me orientar

## Etapa 2 - entender o que era nodejs

Na faculdade mexi com javascript, e um pouco de jquery para fazer algumas coisas com ajax e PHP. Entretanto essas foram minha única experiência com javascript. Vi vários vídeos para entender qual era a lógica por trás e alguns exercício simples.

## Etapa 3 - começar a programar

Demorei um pouco para entender como funciona o **mysql2**, pois não achei muitos tutoriais úteis em relação ao contexto desse teste. Mas tentei me virar.

Comecei tentando popular o **cliente**, depois o **lancamento**. Para isso criei uma pasta chamada **src** que contêm as classes que são relacionadas a cada tabela do modelo físico, e mais um arquivo que retorna uma conexão com o banco. 

Tentei modularizar e documentar o máximo possível para poder ser um código bonito e reutilizável.

### Etapa 3.1 - aventura para popular tabela cliente

Em **cliente** eu fiz um select do **nome**, **email** e **cpf**. Peguei o vetor de objetos que advêm da variável **result** e passei para uma função que faz a inserção no outro banco.

Entretanto eu não consegui fazer duas consultas ao mesmo tempo, e passar junto o código de venda nessa ação no banco de dados.

Pensei em pegar a quantidade de itens da tabela **cliente**, pois sabendo que cada usuário e único iria fazer um update com o código de venda utilizando o id de **cliente**. Porém a tabela que viria o código de venda existe mais de 3000 registros, então essa ideia caiu por água abaixo.

### Etapa 3.1 - aventura para popular tabela lancamento

como dito nas deduções, eu fiz basicamente aquilo

```js 
[...]
this.tempDataVencimento = 'STR_TO_DATE('+ result[i].dataVenc.trim() + ', %d/%m/%Y %H:%i:%s )'
this.tempDataRecebimento = 'STR_TO_DATE('+ result[i].DataRec.trim() + ', %d/%m/%Y %H:%i:%s )'
this.tempDescricao = result[i].descricao+'';
this.parcela = this.tempDescricao.split(" ",1) +''
this.parcelaAtual = parseInt(this.parcela[0], 10)
this.totalParcela = parseInt(this.parcela[2] + this.parcela[3], 10)
this.desconto = parseFloat(result[i].desconto.trim(), 10)
this.valor = parseFloat(result[i].valor.trim(), 10)
[...]
```
Infelizmente o código acima não funcionou pois eu precisaria dos **tipos** que não podem ser nulos, e ter a tabela **cliente** venda já gerada para poder passar o id para lancamentos.

### Conclusão

Eu me diverti muito fazendo esse teste, foi um verdadeiro desafio para mim. Infelizmente eu não conseguir terminar.

Acredito que não foi por falta de tempo, eu fiquei quase 20 horas desde quarta até quinta aprendendo e praticando. Acredito que alguém me orientando sobre as mudanças do banco de dados, com exemplos semelhantes e alguém para perguntar dúvidas seria muito bem vindo. Provavelmente eu teria mais sucesso no teste.

Muito obrigado pela oportunidade do bate papo na terça-feira, foi uma entrevista que eu não fiquei entediado. E também por  proporcionar tanto conhecimento.



